require 'open-uri'
require 'zip'

# Mutex to prevent stdout jumbling
$stdout_mutex = Mutex.new

# Ensure output to stdout is handled properly with multitasks.
# @param args [Array] The arguments to puts.
def puts(*args)
  $stdout_mutex.synchronize { super(*args) }
end


# Expose the rake_require method directly.
# @param args [Array] The args to pass to rake_require.
def rake_require(*args)
  Rake.application.rake_require(*args)
end

# Create a FileTask to download a file and place it in the given destination.
# @param hsh [Hash] Task-style hash of filename and dependencies, including URL.
def download(hsh)
  dest = hsh.keys[0]
  url = nil
  deps = []
  hsh.values.flatten.each do |dep|
    if dep.respond_to?(:include?) and dep.include?('://')
      if url.nil?
        url = dep
      else
        raise 'More than one URL provided to download!'
      end
    else
      deps << dep
    end
  end
  file dest => deps do
    FileUtils.mkdir_p(File.dirname(dest))
    url_contents = open(URI.escape(url)) { |w| w.read }
    File.write(dest, url_contents)
  end
end

def unzip(hsh)
  dest_dir = hsh.keys.first
  src = hsh.values.flatten
  dest_dummy = "#{dest_dir.gsub(/[\\\/]+$/, '')}.dummy"
  file dest_dummy => src do
    FileUtils.rm_rf(dest_dir)
    Zip::File.open(src) do |zf|
      zf.each do |file|
        dest_name = File.join(dest_dir, f.name)
        FileUtils.mkdir_p(File.dirname(dest_name))
        zf.extract(file, dest_name)
      end
    end
    FileUtils.touch(dest_dummy)
  end
end

# Create a multitask and run it immediately.
# @param hsh [Hash] The task-style hash.
# @param block [Block] The block to execute.
def immediate_multitask(hsh, &block)
  multitask(hsh, &block).invoke
end
