require 'nokogiri'
require 'yaml'
require 'utils'
rake_require 'utils'

TRACK_DATA_PATH = File.join(*%w[data track])
TRACK_SOURCE_PATH = File.join(TRACK_DATA_PATH, 'sources')
TRACK_UNZIPPED_PATH = File.join(TRACK_DATA_PATH, 'unzipped')
TRACK_COMBINED_PATH = File.join(TRACK_DATA_PATH, 'combined.yml')
TRACK_COMBINED_XML_DUMMY_PATH = File.join(TRACK_DATA_PATH, 'combined_gpx.dummy')
TRACK_WITH_DATES_PATH = File.join(TRACK_DATA_PATH, 'with_dates.yml')
TRACK_BY_DATE_PATH = File.join(TRACK_DATA_PATH, 'by_date')
TRACK_BY_DATE_DUMMY_PATH = File.join(TRACK_BY_DATE_PATH, 'by_date.dummy')
DATE_CONFIG_FILE = 'dates_cfg.yml'

class Date
  def truncate
    Date.new(year, month, day)
  end
end

def unzipped_prereqs(unzipped_dummy)
  basename = File.basename(unzipped_dummy, '.dummy')
  File.join(TRACK_SOURCE_PATH, "#{basename}.zip")
end

# Download ZIPs
rule Regexp.new(File.join(TRACK_SOURCE_PATH, '\w{2}_state_gps.zip')) do |t|
  url = "https://www.pctmap.net/wp-content/uploads/pct/#{File.basename(t.name)}"
  url_contents = open(URI.escape(url)) { |w| w.read }
  FileUtils.mkdir_p(File.dirname(t.name))
  File.write(t.name, url_contents)
  puts "DOWNLOAD #{t.name}"
end

# Unzip ZIPs
rule Regexp.new(File.join(TRACK_UNZIPPED_PATH, '\w{2}_state_gps.dummy')) => proc { |tn| unzipped_prereqs(tn) } do |t|
  dest_dir = File.join(File.dirname(t.name), File.basename(t.name, '.dummy'))
  FileUtils.rm_rf(dest_dir)
  FileUtils.mkdir_p(dest_dir)
  Zip::File.open(t.prerequisites[0]) do |zf|
    zf.each do |file|
      dest_name = File.join(dest_dir, file.name)
      FileUtils.mkdir_p(File.dirname(dest_name))
      zf.extract(file, dest_name)
    end
  end
  FileUtils.touch(t.name)
  puts "UNZIP #{t.name}"
end

# Collect all gpx tracks into one YAML file
file TRACK_COMBINED_PATH => %w[ca or wa].map { |s| File.join(TRACK_UNZIPPED_PATH, "#{s}_state_gps.dummy") } do |t|
  FileUtils.mkdir_p(File.dirname(t.name))
  prev_pt = nil
  File.write(t.name, Dir[File.join(TRACK_UNZIPPED_PATH, '**', '*_tracks.gpx')].sort.map do |gpx|
    sec_name = File.basename(gpx, '_tracks.gpx').gsub('_', ' ')
    Nokogiri::XML(File.read(gpx)).css('gpx trk').select { |trk| trk.css('name')[0].text == sec_name }[0].css('trkpt').map do |trkpt|
      pt = {
        lat: trkpt['lat'].to_f,
        lon: trkpt['lon'].to_f,
        ele: trkpt.css('ele')[0].text.to_f,
        mile: 0
      }
      prev_pt ||= pt
      pt[:mile] = prev_pt[:mile] + dist(prev_pt, pt)
      prev_pt = pt
      pt
    end
  end.flatten.to_yaml)
  puts "COMBINE #{t.name}"
end

# Add days to the track
file TRACK_WITH_DATES_PATH => [TRACK_COMBINED_PATH, DATE_CONFIG_FILE] do |t|
  track = YAML.load(File.read(TRACK_COMBINED_PATH))
  schedule = YAML.load(File.read(DATE_CONFIG_FILE))

  if schedule.first['mile']
    raise 'Mile may not be provided for the first entry! It is assumed to be 0.'
  else
    schedule.first['mile'] = 0
  end

  if schedule.last['mile']
    raise 'Mile may not be provided for the last entry! It is assumed to be the end.'
  else
    schedule.last['mile'] = track.last[:mile]
  end

  sched_idx = 0
  start_sched_point = schedule[sched_idx]
  end_sched_point = schedule[sched_idx+1]
  track.each do |point|

    # Advance to next schedule section if necessary
    if point[:mile] > end_sched_point['mile']
      sched_idx += 1
      start_sched_point = schedule[sched_idx]
      end_sched_point = schedule[sched_idx+1]
    end

    # Determine date of this point
    sec_frac = (point[:mile] - start_sched_point['mile']) / (end_sched_point['mile'] - start_sched_point['mile'])
    date = start_sched_point['date'] + (sec_frac * (end_sched_point['date'] - start_sched_point['date']))
    point[:date] = date.truncate
  end

  File.write(t.name, track.to_yaml)
end

# Split the track into days
file TRACK_BY_DATE_DUMMY_PATH => TRACK_WITH_DATES_PATH do |t|
  track = YAML.load(File.read(TRACK_WITH_DATES_PATH))
  cfg = YAML.load(File.read(DATE_CONFIG_FILE))
  FileUtils.rm_rf(TRACK_BY_DATE_PATH)
  FileUtils.mkdir_p(TRACK_BY_DATE_PATH)

  total_miles = track[-1][:mile]
  cur_date_array = []
  cur_date = track.first[:date]
  track.each do |point|
    if point[:date] != cur_date
      File.write(File.join(TRACK_BY_DATE_PATH, "#{cur_date}.yml"), cur_date_array.to_yaml)
      cur_date = point[:date]
      cur_date_array = []
    end
    cur_date_array << point.reject { |k, v| k == :date }
  end
  File.write(File.join(TRACK_BY_DATE_PATH, "#{cur_date}.yml"), cur_date_array.to_yaml)

  FileUtils.touch(TRACK_BY_DATE_DUMMY_PATH)
  puts "SEPARATE #{t.name}"
end

rule Regexp.new(File.join(TRACK_BY_DATE_PATH, '\d{4}-\d{2}-\d{2}\.yml')) => TRACK_BY_DATE_DUMMY_PATH
