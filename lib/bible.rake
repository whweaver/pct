require 'yaml'

BIBLE_DATA_PATH = File.join(*%w[data bible])
BIBLE_RAW_VERSES_PATH = File.join(BIBLE_DATA_PATH, 'raw_verses.txt')
BIBLE_PARSED_PATH = File.join(BIBLE_DATA_PATH, 'bible.yml')
BIBLE_OT_PARSED_PATH = File.join(BIBLE_DATA_PATH, 'ot.yml')
BIBLE_NT_PARSED_PATH = File.join(BIBLE_DATA_PATH, 'nt.yml')

def slice_bible(bible, start_book, end_book)
  books = bible.keys
  bible.slice(*books[books.find_index(start_book)..books.find_index(end_book)])
end

# Parse raw verses
file BIBLE_PARSED_PATH => BIBLE_RAW_VERSES_PATH do |t|
  books = {}
  File.foreach(BIBLE_RAW_VERSES_PATH) do |raw_line|
    tokenized_line = raw_line.split
    num_words = 1
    book_name = tokenized_line[1]
    tokenized_line[2..-1].each do |word|
      if word =~ /^\d+$/
        break
      else
        book_name << " #{word}"
        num_words += 1
      end
    end
    books[book_name] = tokenized_line[(3 + num_words)..-1].map(&:to_i)
  end

  FileUtils.mkdir_p(File.dirname(t.name))
  File.write(t.name, books.to_yaml)
end

# Split Bible into OT
file BIBLE_OT_PARSED_PATH => BIBLE_PARSED_PATH do |t|
  FileUtils.mkdir_p(File.dirname(t.name))
  File.write(t.name, slice_bible(YAML.load(File.read(BIBLE_PARSED_PATH)), 'Genesis', 'Malachi').to_yaml)
end

# Split Bible into NT
file BIBLE_NT_PARSED_PATH => BIBLE_PARSED_PATH do |t|
  FileUtils.mkdir_p(File.dirname(t.name))
  File.write(t.name, slice_bible(YAML.load(File.read(BIBLE_PARSED_PATH)), 'Matthew', 'Revelation').to_yaml)
end
