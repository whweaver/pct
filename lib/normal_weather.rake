require 'open-uri'
require 'yaml'
require 'delau'
require 'geographic_interpolation'
rake_require 'utils'

NORMAL_WEATHER_DATA_PATH = File.join(*%w[data normal_weather])
NORMAL_WEATHER_SOURCE_PATH = File.join(NORMAL_WEATHER_DATA_PATH, 'sources')
NORMAL_WEATHER_PARSED_PATH = File.join(NORMAL_WEATHER_DATA_PATH, 'parsed')
NORMAL_WEATHER_INTERPOLATOR_PATH =File.join(NORMAL_WEATHER_DATA_PATH, 'interpolators')
NORMAL_WEATHER_STATIONS_SOURCE = File.join(NORMAL_WEATHER_SOURCE_PATH, 'allstations.txt')
NORMAL_WEATHER_STATIONS_PARSED = File.join(NORMAL_WEATHER_PARSED_PATH, 'allstations.yml')
DEGREES_F_PER_METER = (((3.3 + 5.4) / 2) / 1000) * 3.28084

def get_interpolator_path(datum, stat, month, day)
  File.join(NORMAL_WEATHER_INTERPOLATOR_PATH, "#{datum}-#{stat}-#{month}-#{day}.yml")
end

def get_interpolator_info(filename)
  File.basename(filename, '.yml').split('-')
end

def get_parsed_prereqs(parsed_path)
  basename = File.basename(parsed_path, '.dummy')
  [File.join(NORMAL_WEATHER_SOURCE_PATH, "#{basename}.txt"), NORMAL_WEATHER_STATIONS_PARSED]
end

def get_interpolator_prereqs(interpolator_path)
  datum, stat = File.basename(interpolator_path, '.yml').split('-')
  File.join(NORMAL_WEATHER_PARSED_PATH, "#{datum}-#{stat}.dummy")
end

def get_triangulation_prereqs(triangulation_path)
  datum, stat = File.basename(triangulation_path, '.yml').split('-')
  File.join(NORMAL_WEATHER_PARSED_PATH, "#{datum}-#{stat}.dummy")
end

# Download station information
download NORMAL_WEATHER_STATIONS_SOURCE => 'https://www1.ncdc.noaa.gov/pub/data/normals/1981-2010/station-inventories/allstations.txt'

# Parse stations
file NORMAL_WEATHER_STATIONS_PARSED => NORMAL_WEATHER_STATIONS_SOURCE do |t|
  stations = {}
  File.foreach(File.join(NORMAL_WEATHER_SOURCE_PATH, 'allstations.txt')) do |line|
    fields = line.split
    stations[fields[0]] = {
      lat: fields[1].to_f,
      lon: fields[2].to_f,
      ele: fields[3].to_f
    }
  end
  FileUtils.mkdir_p(File.dirname(t.name))
  File.write(t.name, stations.to_yaml)
  puts "PARSE #{t.name}"
end

# Download datum/stat
rule Regexp.new(File.join(NORMAL_WEATHER_SOURCE_PATH, '\w+-\w+\.txt')) do |t|
  url = "https://www1.ncdc.noaa.gov/pub/data/normals/1981-2010/products/temperature/dly-#{File.basename(t.name)}"
  url_contents = open(URI.escape(url)) { |w| w.read }
  FileUtils.mkdir_p(File.dirname(t.name))
  File.write(t.name, url_contents)
  puts "DOWNLOAD #{t.name}"
end

# Parse datum/stat
rule Regexp.new(File.join(NORMAL_WEATHER_PARSED_PATH, '\w+-\w+\.dummy')) => proc { |tn| get_parsed_prereqs(tn) } do |t|
  stations = YAML.load(File.read(NORMAL_WEATHER_STATIONS_PARSED))
  basename = File.basename(t.name, '.dummy')
  datum, stat = basename.split('-')
  data = {}
  File.foreach(t.prerequisites.first) do |line|
    fields = line.split
    station_name = fields[0]
    station = stations[station_name]
    if station
      month = fields[1].to_i
      data[month] ||= {}
      fields[2..-1].each_with_index do |value, idx|
        unless value == '-8888'
          day = idx + 1
          data[month][day] ||= []

          value = value.to_f / 10.0
          value = value + (DEGREES_F_PER_METER * station[:ele]) if stat == 'normal'

          data[month][day] << {
            name: station_name,
            lat: station[:lat],
            lon: station[:lon],
            value: value
          }
        end
      end
    else
      puts "Unknown station while parsing #{t.name}: #{station_name}"
    end
  end
  FileUtils.mkdir_p(File.dirname(t.name))

  # Write individual day data
  data.each do |month, month_data|
    month_data.each do |day, day_data|
      File.write(File.join(File.dirname(t.name), "#{basename}-#{month}-#{day}.yml"), day_data.to_yaml)
    end
  end

  FileUtils.touch(t.name)
  puts "PARSE #{t.name}"
end

# Generate interpolators
rule Regexp.new(File.join(NORMAL_WEATHER_INTERPOLATOR_PATH, '\w+-\w+-\d+-\d+.yml')) => proc { |tn| get_interpolator_prereqs(tn) } do |t|
  FileUtils.mkdir_p(File.dirname(t.name))
  basename = File.basename(t.name, '.yml')
  parsed_data = YAML.load(File.read(File.join(NORMAL_WEATHER_PARSED_PATH, "#{basename}.yml")))
  File.write(t.name, GeographicInterpolation.new(parsed_data).to_yaml)
  puts "INTERPOLATOR #{t.name}"
end
