class ColorSpectrum
  SET_POINTS = [{ red: 255, green:   0, blue: 255 },
                { red:   0, green:   0, blue: 255 },
                { red:   0, green: 255, blue: 255 },
                { red:   0, green: 255, blue:   0 },
                { red: 255, green: 255, blue:   0 },
                { red: 255, green:   0, blue:   0 },
                { red: 255, green:   0, blue: 255 }]

  def self.get_color(frac)
    color_region = (frac * (SET_POINTS.length - 1))
    color_ceil = SET_POINTS[color_region.ceil]
    color_floor = SET_POINTS[color_region.floor]
    color_region_frac = color_region - color_floor
    %i[red green blue].map { |component| color_floor[component] + color_region_frac * (color_ceil[component] - color_floor[component]) }
  end
end
