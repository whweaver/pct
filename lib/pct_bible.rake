require 'yaml'
rake_require 'bible'
rake_require 'track'

TRACK_WITH_BIBLE_PATH = File.join(BIBLE_DATA_PATH, 'track_with_bible.yml')
TRACK_WITH_BIBLE_BOOKS_GPX = File.join(BIBLE_DATA_PATH, 'track_with_bible_books.gpx')
TRACK_WITH_BIBLE_REGEXP = Regexp.new(File.join(BIBLE_DATA_PATH, 'track_with_(.*)\.yml'))
TRACK_WITH_BIBLE_GPX_REGEXP = Regexp.new(File.join(BIBLE_DATA_PATH, '(track_with_.*)\.gpx'))

# Add bible info to track
rule TRACK_WITH_BIBLE_REGEXP => ->(tn){ [TRACK_COMBINED_PATH, File.join(BIBLE_DATA_PATH, "#{tn.match(TRACK_WITH_BIBLE_REGEXP)[1]}.yml")] } do |t|
  track = YAML.load(File.read(TRACK_COMBINED_PATH))
  bible = YAML.load(File.read(t.prerequisites[1]))

  # Compute total stats
  total_verses = bible.reduce(0) { |total, (book, chapters)| chapters.sum(total) }
  total_miles = track.last[:mile]
  verses_per_mile = total_verses / total_miles

  # Add Bible data to track
  books = bible.keys
  cur_book_idx = 0
  cur_book = books[cur_book_idx]
  cur_chapter = 1
  cur_verse = 1
  last_mile = 0
  track.each do |point|
    new_verses = (point[:mile] - last_mile) * verses_per_mile
    cur_verse += new_verses

    # Roll over to new chapter
    if cur_verse > bible[cur_book][cur_chapter-1]
      cur_verse -= bible[cur_book][cur_chapter-1]
      cur_chapter += 1
    end

    # Roll over to new book
    if cur_chapter > bible[cur_book].length
      cur_chapter -= bible[cur_book].length
      cur_book_idx += 1
      cur_book = books[cur_book_idx]
    end

    # Truncate to end of Bible
    if cur_book_idx >= books.length
      cur_book_idx = books.length - 1
      cur_book = books[cur_book_idx]
      cur_chapter = bible[cur_book].length
      cur_verse = bible[cur_book][cur_chapter-1]
    end

    # Add bible info to point
    point[:book] = cur_book
    point[:chapter] = cur_chapter.to_i
    point[:verse] = cur_verse.to_i

    # Update state
    last_mile = point[:mile]
  end

  # Write results to file
  FileUtils.mkdir_p(File.dirname(t.name))
  File.write(t.name, track.to_yaml)
end

# Generate GPX file with bible book segments
rule TRACK_WITH_BIBLE_GPX_REGEXP => ->(tn){ File.join(BIBLE_DATA_PATH, "#{tn.match(TRACK_WITH_BIBLE_GPX_REGEXP)[1]}.yml") } do |t|
  # Load files
  track = YAML.load(File.read(t.prerequisites[0]))

  # Split track into books
  subtracks = {}
  track.each do |point|
    subtracks[point[:book]] ||= []
    subtracks[point[:book]] << point
  end

  # Build GPX
  gpx_builder = Nokogiri::XML::Builder.new do |xml|
    xml.gpx(xmlsn: 'http://www.topografix.com/GPX/1/1',
            creator: 'Will Weaver',
            version: '1.1',
            'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'xsi:schemaLocation' => 'http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd') do
      subtracks.each do |book, subtrack|
        xml.trk do
          xml.name(book)
          xml.trkseg do
            subtrack.each do |point|
              xml.trkpt(lat: point[:lat], lon: point[:lon])
            end
          end
        end
      end
    end
  end

  # Write file
  FileUtils.mkdir_p(File.dirname(t.name))
  File.write(t.name, gpx_builder.to_xml.gsub(/\n\s+/, ''))
end
