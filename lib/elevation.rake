require 'yaml'
rake_require 'track'

ELEVATION_DIR_PATH = File.join(*%w[data elevation])
ELEVATION_PROFILE_CSV = File.join(ELEVATION_DIR_PATH, 'profile.csv')
ELEVATION_GRADE_CSV = File.join(ELEVATION_DIR_PATH, 'grade.csv')
ELEVATION_DAILY_GRADE_STATS_CSV = File.join(ELEVATION_DIR_PATH, 'daily_grade_stats.csv')

file ELEVATION_PROFILE_CSV => TRACK_COMBINED_PATH do |t|
  track = YAML.load(File.read(TRACK_COMBINED_PATH))

  FileUtils.mkdir_p(File.dirname(t.name))
  File.open(t.name, 'w') do |f|
    track.each do |point|
      f << "#{point[:mile]},#{point[:ele]}\n"
    end
  end
end

file ELEVATION_GRADE_CSV => TRACK_COMBINED_PATH do |t|
  track = YAML.load(File.read(TRACK_COMBINED_PATH))

  FileUtils.mkdir_p(File.dirname(t.name))
  prev_point = nil
  File.open(t.name, 'w') do |f|
    track.each do |point|
      if prev_point
        grade = (point[:ele] - prev_point[:ele]) / (point[:mile] - prev_point[:mile])
        mile = (point[:mile] + prev_point[:mile]) / 2
        f << "#{mile},#{grade}\n"
      end
      prev_point = point
    end
  end
end

file ELEVATION_DAILY_GRADE_STATS_CSV => TRACK_BY_DATE_DUMMY_PATH do |t|
  FileUtils.mkdir_p(File.dirname(t.name))
  File.open(t.name, 'w') do |f|
    FileList.new(File.join(TRACK_BY_DATE_PATH, '*.yml')).each_with_index do |date_file, idx|
      date_track = YAML.load(File.read(date_file))
      date = date_track[0][:date]
      prev_point = nil
      max_ascent = 0
      max_descent = 0
      total_ascent = 0
      ascent_miles = 0
      total_descent = 0
      descent_miles = 0
      total = 0
      total_miles = 0
      date_track.each do |point|
        if prev_point
          meters = point[:ele] - prev_point[:ele]
          miles = point[:mile] - prev_point[:mile]
          grade = meters / miles
          max_ascent = grade if grade > max_ascent
          max_descent = grade if grade < max_descent
          total += meters
          total_miles += miles
          if grade > 0
            total_ascent += meters
            ascent_miles += miles
          elsif grade < 0
            total_descent += meters
            descent_miles += miles
          end
        end
        prev_point = point
      end
      avg_ascent = total_ascent / ascent_miles
      avg_descent = total_descent / descent_miles
      avg = total / total_miles
      f << "#{idx},#{max_descent},#{avg_descent},#{avg},#{avg_ascent},#{max_ascent}\n"
    end
  end
end
