def haversine(pt1, pt2)
  lat1 = pt1[:lat] * Math::PI / 180
  lon1 = pt1[:lon] * Math::PI / 180
  lat2 = pt2[:lat] * Math::PI / 180
  lon2 = pt2[:lon] * Math::PI / 180
  2 * 3958.8 * Math.asin(Math.sqrt(Math.sin((lat1 - lat2) / 2) ** 2 + Math.cos(lat1) * Math.cos(lat2) * Math.sin((lon1 - lon2) / 2) ** 2))
end

def dist(pt1, pt2)
  1.025 * haversine(pt1, pt2)
end
