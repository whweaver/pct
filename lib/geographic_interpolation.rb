require 'yaml'
require 'delau'

# Represents a set of values at geographic coordinates from which
# values at any other point can be interpolated.
class GeographicInterpolation
  public

  # Creates a GeographicInterpolation from its YAML representation.
  # @param yaml [String] The YAML representation of the interpolation.
  # @return [GeographicInterpolation] The interpolation.
  def self.from_yaml(yaml)
    GeographicInterpolation.new(yaml)
  end

  # @return [String] A YAML representation of the interpolation
  def to_yaml
    # Create a list of points that have no reference to each other
    flat_points = []
    flat_pt_idx_by_deep_pt = {}
    @latice_by_lat.each_with_index do |deep_point, idx|
      flat_pt_idx_by_deep_pt[deep_point] = idx
      flat_points << deep_point.slice(*%i[name lat lon value])
    end

    # Add adjacent mappings by referencing point indices rather than the points themselves
    @latice_by_lat.each do |deep_point|
      flat_pt_idx = flat_pt_idx_by_deep_pt[deep_point]
      flat_pt = flat_points[flat_pt_idx]
      flat_pt[:adj_pts] = deep_point[:adj_pts].map do |adj_deep_point|
        flat_pt_idx_by_deep_pt[adj_deep_point]
      end
    end

    # Convert longitude-sorted list into a list of indices from the latitude-sorted list
    flat_by_lon = @latice_by_lon.map { |deep_point| flat_pt_idx_by_deep_pt[deep_point] }

    {
      by_lat: flat_points,
      by_lon: flat_by_lon
    }.to_yaml
  end

  # Determines the value at the specified point.
  # @param lat [Numeric] The latitude of the point.
  # @param lon [Numeric] The longitude of the point.
  # @return [Numeric] The interpolated value at the requested point.
  def interpolate(lat, lon)
    int_point = {lat: lat, lon: lon}

    # Use the nearest point to the interpolation point as a starting point
    candidate_point = find_closest_point(int_point)
    cw_pt = nil
    ccw_pt = nil

    # Find the Delaunay triangle the point resides in
    loop do

      # Find the wedge from the candidate point this point resides in
      ccw_pt_idx = candidate_point[:adj_pts].find_index { |adj_pt| is_cw_of_line(candidate_point, adj_pt, int_point) }
      ccw_pt_idx = 0 if ccw_pt_idx.nil?
      ccw_pt = candidate_point[:adj_pts][ccw_pt_idx]
      cw_pt = candidate_point[:adj_pts][ccw_pt_idx - 1]

      # Break if the interpolated point is in the triangle formed by the points defining the wedge
      break if is_cw_of_line(ccw_pt, cw_pt, int_point)

      # Use nearest of the two ray points as the next candidate point
      candidate_point = (haversine(int_point, cw_pt) < haversine(int_point, ccw_pt)) ? cw_pt : ccw_pt
    end

    # Use a plane defined by the Delaunay triangle to interpolate the value of the point
    interpolate_from_points(candidate_point, ccw_pt, cw_pt, int_point)
  end

  private

  # Initializer
  # All angles in degrees.
  # @param latice [Array<Hash>] Array of datapoints in the format {lat: <Numeric>, lon: <Numeric>, value: <Numeric>}
  def init_from_latice(latice)

    # Create sorted lists of the latice by latitude and longitude
    @latice_by_lat = latice.sort_by { |point| point[:lat] }
    @latice_by_lon = latice.sort_by { |point| point[:lon] }

    # Find the Delaunay Triangulation of the latice
    triangulation_vertices = Delau.deltri(*latice.map { |a| [a[:lon], a[:lat]] }.transpose)[:v]

    # Add Delaunay Triangulation data to latice points
    triangles = triangulation_vertices[0].zip(*triangulation_vertices[1..2])
    triangles.each do |triangle|
      triangle.each do |v1_idx|
        p1 = latice[v1_idx]
        p1[:adj_pts] ||= []
        triangle.each do |v2_idx|
          p2 = latice[v2_idx]
          p1[:adj_pts] << p2 unless p1[:adj_pts].include?(p2) or p1 == p2
        end
      end
    end

    # Sort adjacent indices by latitude in the triangulation
    latice.each do |point|
      point[:adj_pts].sort_by! { |adj_pt| angle_from_point(point, adj_pt) }
    end
  end

  # Initializer.
  # @param yaml [String] The yaml describing the interpolation.
  def init_from_yaml(yaml)
    hsh = YAML.load(yaml)

    hsh[:by_lat].each do |flat_pt|
      flat_pt[:adj_pts].map! do |adj_pt_idx|
        hsh[:by_lat][adj_pt_idx]
      end
    end

    @latice_by_lat = hsh[:by_lat]
    @latice_by_lon = hsh[:by_lon].map { |idx| hsh[:by_lat][idx] }
  end

  # Delegates to the appropriate initializer based on type of arguments.
  # @param args [Array] The arguments to the initializer.
  def initialize(arg)
    if arg.is_a?(Array)
      init_from_latice(arg)
    elsif arg.is_a?(String)
      init_from_yaml(arg)
    else
      raise "Unknown argument: #{arg}"
    end
  end

  # Calculate the haversine distance between two points
  # @param pt1 [Hash] The first point (degrees).
  # @param pt2 [Hash] The second point (degrees).
  # @return [Numeric] The haversine distance between the two points.
  def haversine(pt1, pt2)
    lat1 = pt1[:lat] * Math::PI / 180
    lon1 = pt1[:lon] * Math::PI / 180
    lat2 = pt2[:lat] * Math::PI / 180
    lon2 = pt2[:lon] * Math::PI / 180
    2 * 3958.8 * Math.asin(Math.sqrt(Math.sin((lat1 - lat2) / 2) ** 2 + Math.cos(lat1) * Math.cos(lat2) * Math.sin((lon1 - lon2) / 2) ** 2))
  end

  # Find the closest latice point to an interpolated point.
  # @param int_point [Hash] The interpolated point to find.
  # @return [Hash] The latice point closest to this point.
  def find_closest_point(int_point)
    closest_lat_idx = @latice_by_lat.bsearch_index { |pt| pt[:lat] >= int_point[:lat] }
    closest_lon_idx = @latice_by_lon.bsearch_index { |pt| pt[:lon] >= int_point[:lon] }

    closest_lat_idx ||= 0
    closest_lon_idx ||= 0

    probes = [
      { idx: closest_lat_idx, inc:  1, limit:   90, list: @latice_by_lat, key: :lat },
      { idx: closest_lat_idx, inc: -1, limit:  -90, list: @latice_by_lat, key: :lat },
      { idx: closest_lon_idx, inc:  1, limit:  180, list: @latice_by_lon, key: :lon },
      { idx: closest_lon_idx, inc: -1, limit: -180, list: @latice_by_lon, key: :lon }
    ]
    nearest_point = nil
    nearest_dist = 24000
    i = 0

    while not probes.empty?
      i += 1
      probes.each do |probe|
        point = probe[:list][probe[:idx]]
        if point and ((probe[:inc] > 0) ? (point[probe[:key]] <= probe[:limit]) : (point[probe[:key]] >= probe[:limit]))
          dist = haversine(point, int_point)
          if dist < nearest_dist
            nearest_point = point
            nearest_dist = dist
            probes.each { |update_probe| update_probe[:limit] = send("#{update_probe[:key]}_limit", int_point, nearest_dist, update_probe[:inc]) }
          end
          probe[:idx] += probe[:inc]
        else
          probes.delete(probe)
          break
        end
      end
    end

    nearest_point
  end

  # Find the farthest latitude within a distnace from a point.
  # @param point [Hash] The point (degrees) from which the latitude is computed.
  # @param dist [Numeric] The distance (miles) from the point.
  # @param inc [Numeric] The direction (+/-) from the point.
  def lat_limit(point, dist, inc)
    delta = dist / 3958.8
    delta_deg = delta * 180 / Math::PI
    (inc > 0) ? point[:lat] + delta_deg : point[:lat] - delta_deg
  end

  # Find the farthest longitude within a distnace from a point.
  # @param point [Hash] The point (degrees) from which the longitude is computed.
  # @param dist [Numeric] The distance (miles) from the point.
  # @param inc [Numeric] The direction (+/-) from the point.
  def lon_limit(point, dist, inc)
    lat_rad = point[:lat] * Math::PI / 180
    delta = 2 * Math.asin(Math.sin(dist / (2 * 3958.8)) / Math.cos(lat_rad))
    delta_deg = delta * 180 / Math::PI
    (inc > 0) ? point[:lon] + delta_deg : point[:lon] - delta_deg
  end

  # Determine if a test point is clockwise of the line defined by two other points.
  # @param line_pt1 [Hash] The base point.
  # @param line_pt2 [Hash] The point defining the line angle.
  # @param test_pt [Hash] The point to test against the line.
  # @param [Boolean] True if the point is clockwise of the line, false otherwise.
  def is_cw_of_line(base_pt, line_pt, test_pt)
    line_angle = angle_from_point(base_pt, line_pt)
    test_angle = angle_from_point(base_pt, test_pt)
    (test_angle < line_angle)
  end

  # Get the angle between two points (in radians).
  # @param base_pt [Hash] The point acting as the center of the coordinate system.
  # @param angle_pt [Hash] The point whose angle to get relative to the base_pt.
  def angle_from_point(base_pt, angle_pt)
    opposite = angle_pt[:lat] - base_pt[:lat]
    adjacent = angle_pt[:lon] - base_pt[:lon]

    # Get an angle in [-PI/2, PI/2]
    if adjacent == 0
      angle = (opposite > 0) ? (Math::PI / 2) : (-Math::PI / 2)
    else
      angle = Math.atan(opposite / adjacent)
    end

    # Get the correct angle in [-PI/2, 3*PI/2]
    angle = Math::PI + angle if adjacent < 0

    # Normalize the angle to [0, 2*PI)
    angle = 2 * Math::PI + angle if angle < 0

    angle
  end

  # Interpolates a value between 3 points, assuming a plane.
  # @param pt1 [Hash] Point 1.
  # @param pt2 [Hash] Point 2.
  # @param pt3 [Hash] Point 3.
  # @param int_pt [Hash] Point to interpolate.
  def interpolate_from_points(pt1, pt2, pt3, int_pt)
    b = (pt3[:value] - pt1[:value] - (pt2[:value] - pt1[:value]) * (pt3[:lon] - pt1[:lon]) / (pt2[:lon] - pt1[:lon])) / (pt3[:lat] - pt1[:lat] - (pt2[:lat] - pt1[:lat]) * (pt3[:lon] - pt1[:lon]) / (pt2[:lon] - pt1[:lon]))
    a = (pt2[:value] - pt1[:value] - b * (pt2[:lat] - pt1[:lat])) / (pt2[:lon] - pt1[:lon])
    c = pt1[:value] - a * pt1[:lon] - b * pt1[:lat]

    a * int_pt[:lon] + b * int_pt[:lat] + c
  end
end
