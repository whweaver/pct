require 'yaml'
require 'date'
require 'chunky_png'
rake_require 'track'
rake_require 'normal_weather'

PCT_WEATHER_DATE_CFG = YAML.load(File.read(DATE_CONFIG_FILE))
PCT_WEATHER_DATE_RANGE = (PCT_WEATHER_DATE_CFG.first['date']..PCT_WEATHER_DATE_CFG.last['date'])
PCT_WEATHER_DATA_PATH = File.join(*%w[data pct_weather])
PCT_WEATHER_TEMP_CFG_PATH = 'temp_cfg.yml'
PCT_WEATHER_TRACK_WITH_WEATHER_STATS_PATH = File.join(PCT_WEATHER_DATA_PATH, 'track_with_weather_stats')
PCT_WEATHER_TRACK_WITH_TEMP_PROBS_PATH = File.join(PCT_WEATHER_DATA_PATH,  'track_with_temp_probs')
PCT_WEATHER_DAILY_WEATHER_PROBS = File.join(PCT_WEATHER_DATA_PATH, 'daily_temp_probs')
PCT_WEATHER_IMAGE_GRID = File.join(PCT_WEATHER_DATA_PATH, 'image_grid')
PCT_WEATHER_PROB_TRACE_PATH = File.join(PCT_WEATHER_DATA_PATH, 'prob_trace')
PCT_WEATHER_SLEEP_COMFORT_PATH = File.join(PCT_WEATHER_DATA_PATH, 'sleep_comfort')
IMAGE_CONFIG_FILE = 'image_cfg.yml'
COLOR_CONFIG_FILE = 'color_cfg.yml'
STATS_CONFIG_FILE = 'stats_cfg.yml'

def with_weather_stats_prereqs(tn)
  month, day = *File.basename(tn, '.yml').split('-').map(&:to_i)[1..-1]
  [
    File.join(TRACK_BY_DATE_PATH, File.basename(tn)),
    get_interpolator_path('tmax', 'normal', month, day),
    get_interpolator_path('tmax', 'stddev', month, day),
    get_interpolator_path('tmin', 'normal', month, day),
    get_interpolator_path('tmin', 'stddev', month, day),
  ]
end

def with_temp_probs_prereqs(tn)
  [File.join(PCT_WEATHER_TRACK_WITH_WEATHER_STATS_PATH, File.basename(tn)), PCT_WEATHER_TEMP_CFG_PATH]
end

def daily_weather_probs_prereqs(tn)
  File.join(PCT_WEATHER_TRACK_WITH_TEMP_PROBS_PATH, File.basename(tn))
end

def image_grid_prereqs(tn)
  [File.join(PCT_WEATHER_TRACK_WITH_TEMP_PROBS_PATH, "#{File.basename(tn, '.csv')}.yml"), IMAGE_CONFIG_FILE, PCT_WEATHER_TEMP_CFG_PATH]
end

def prob_trace_prereqs(tn)
  [STATS_CONFIG_FILE, File.join(PCT_WEATHER_TRACK_WITH_WEATHER_STATS_PATH, "#{File.basename(tn, '.csv')}.yml")]
end

def sleep_comfort_prereqs(tn)
  basename = File.basename(tn, '.csv')
  next_date = Date.parse(basename) - 1
  pr = [DATE_CONFIG_FILE, STATS_CONFIG_FILE, File.join(PCT_WEATHER_TRACK_WITH_WEATHER_STATS_PATH, "#{basename}.yml")]
  if PCT_WEATHER_DATE_RANGE.include?(next_date)
    pr << File.join(PCT_WEATHER_TRACK_WITH_WEATHER_STATS_PATH, "#{next_date}.yml")
  end
  pr
end

def probability_threshold(point, thresholds)
  prev_temp = nil
  prev_prob = 0
  prob_trace = { asc: {}, desc: {} }

  thresholds.each do |thresh_prob|
    z_score = Distribution::Normal.p_value(thresh_prob)
    prob_trace[:desc][thresh_prob] = point[:tmax][:normal] - (z_score * point[:tmax][:stddev])
    prob_trace[:asc][thresh_prob] = point[:tmin][:normal] + (z_score * point[:tmin][:stddev])
  end

  prob_trace
end

# Add weather data to each point
rule Regexp.new(File.join(PCT_WEATHER_TRACK_WITH_WEATHER_STATS_PATH, '\d{4}-\d{2}-\d{2}\.yml')) => proc { |tn| with_weather_stats_prereqs(tn) } do |t|
  basename = File.basename(t.name, '.yml')
  date = Date.parse(basename)
  interpolator_filenames = t.prerequisites[1..-1]
  interpolators = interpolator_filenames.map { |pr| [pr, GeographicInterpolation.from_yaml(File.read(pr))] }
  subtrack = YAML.load(File.read(t.prerequisites[0]))
  subtrack.each do |point|
    interpolators.each do |fn, int|
      datum, stat = get_interpolator_info(fn)
      point[datum.to_sym] ||= {}
      point[datum.to_sym][stat.to_sym] = int.interpolate(point[:lat], point[:lon])
      point[datum.to_sym][stat.to_sym] -= DEGREES_F_PER_METER * point[:ele] if stat == 'normal'
    end
  end
  FileUtils.mkdir_p(File.dirname(t.name))
  File.write(t.name, subtrack.to_yaml)
  puts "WEATHER #{t.name}"
end

# Add temp probabilities to each point
rule Regexp.new(File.join(PCT_WEATHER_TRACK_WITH_TEMP_PROBS_PATH, '\d{4}-\d{2}-\d{2}\.yml')) => proc { |tn| with_temp_probs_prereqs(tn) } do |t|
  subtrack = YAML.load(File.read(t.prerequisites[0]))
  cfg = YAML.load(File.read(t.prerequisites[1]))
  range = (cfg['min_temp']..cfg['max_temp']).step(cfg['resolution'])
  subtrack.each do |point|
    point[:temp_probs] = Hash[range.map do |temp|
      prob_tmax_is_higher = 1 - Distribution::Normal.cdf((temp - point[:tmax][:normal]) / point[:tmax][:stddev])
      prob_tmin_is_lower = Distribution::Normal.cdf((temp - point[:tmin][:normal]) / point[:tmin][:stddev])
      [temp, prob_tmax_is_higher * prob_tmin_is_lower]
    end]
  end
  FileUtils.mkdir_p(File.dirname(t.name))
  File.write(t.name, subtrack.to_yaml)
  puts "PROBABILITY #{t.name}"
end

# Generate expected days for each temperature
rule Regexp.new(File.join(PCT_WEATHER_DAILY_WEATHER_PROBS, '\d{4}-\d{2}-\d{2}\.yml')) => proc { |tn| daily_weather_probs_prereqs(tn) } do |t|
  expected_days = {}
  date_data = YAML.load(File.read(t.prerequisites[0]))
  total_miles = date_data.last[:mile] - date_data.first[:mile]
  date_data.each_with_index do |point, idx|
    last_mile = (idx > 0) ? date_data[idx-1][:mile] : date_data[0][:mile]
    next_mile = (idx < date_data.length - 1) ? date_data[idx+1][:mile] : date_data.last[:mile]
    miles_covered = (next_mile + point[:mile]) / 2 - (last_mile + point[:mile]) / 2
    point[:temp_probs].each do |temp, prob|
      expected_days[temp] ||= 0
      expected_days[temp] += prob * miles_covered / total_miles
    end
  end
  FileUtils.mkdir_p(File.dirname(t.name))
  File.write(t.name, expected_days.to_yaml)
  puts "STATISTIC #{t.name}"
end

# Generate expected days for each temperature (combined)
file File.join(PCT_WEATHER_DATA_PATH, 'expected_days.yml') => [DATE_CONFIG_FILE] + PCT_WEATHER_DATE_RANGE.map { |date| File.join(PCT_WEATHER_DAILY_WEATHER_PROBS, "#{date.to_s}.yml") } do |t|
  expected_days = {}
  t.prerequisites[1..-1].each do |date_file|
    YAML.load(File.read(date_file)).each do |temp, prob|
      expected_days[temp] ||= 0
      expected_days[temp] += prob unless prob.nan?
    end
  end
  FileUtils.mkdir_p(File.dirname(t.name))
  File.write(t.name, expected_days.to_yaml)
  puts "STATISTIC #{t.name}"
end

# Generate probability threshold temperature CSVs
rule Regexp.new(File.join(PCT_WEATHER_PROB_TRACE_PATH, '\d{4}-\d{2}-\d{2}\.csv')) => proc { |tn| prob_trace_prereqs(tn) } do |t|
  cfg = YAML.load(File.read(t.prerequisites[0]))
  track = YAML.load(File.read(t.prerequisites[1]))
  FileUtils.mkdir_p(File.dirname(t.name))
  File.open(t.name, 'w') do |f|

    # Write header to file
    asc_probs = cfg['prob_lines'].sort
    desc_probs = asc_probs.reverse
    thresholds = asc_probs + desc_probs
    f << "Mile,#{thresholds.join(',')}\n"

    # Get the temperatures for each probability threshold for each point
    track.each do |point|

      prob_trace = probability_threshold(point, cfg['prob_lines'])

      # Write the point's line to the CSV file
      f << "#{point[:mile]},"
      f << asc_probs.map { |prob| prob_trace[:asc][prob] }.join(',')
      f << ','
      f << desc_probs.map { |prob| prob_trace[:desc][prob] }.join(',')
      f << "\n"
    end
  end

  puts "STATISTIC #{t.name}"
end

# Combine probability threshold temperature CSVs
file File.join(PCT_WEATHER_DATA_PATH, 'prob_trace.csv') => [DATE_CONFIG_FILE] + PCT_WEATHER_DATE_RANGE.map { |date| File.join(PCT_WEATHER_PROB_TRACE_PATH, "#{date.to_s}.csv") } do |t|
  FileUtils.mkdir_p(File.dirname(t.name))
  File.open(t.name, 'w') do |f|
    t.prerequisites[1..-1].each do |day|
      File.readlines(day)[1..-1].each do |day_line|
        f << day_line
      end
    end
  end

  puts "COMBINE #{t.name}"
end

# Generate best possible sleep comfort CSVs
rule Regexp.new(File.join(PCT_WEATHER_SLEEP_COMFORT_PATH, '\d{4}-\d{2}-\d{2}\.csv')) => proc { |tn| sleep_comfort_prereqs(tn) } do |t|
  cfg = YAML.load(File.read(t.prerequisites[1]))
  track = YAML.load(File.read(t.prerequisites[2]))
  if t.prerequisites[3]
    prev_track = YAML.load(File.read(t.prerequisites[3]))
    miles_covered = (track.last[:mile] - prev_track.first[:mile]) / 2
  else
    prev_track = nil
    miles_covered = track.last[:mile] - track.first[:mile]
  end

  # Fill up running average with previous track
  if prev_track
    running_avg_track = prev_track.map do |point|
      prob_trace = probability_threshold(point, cfg['prob_lines'])
      point.slice(:mile).merge({ prob_trace: prob_trace[:asc] })
    end
  else
    running_avg_track = []
  end

  asc_probs = cfg['prob_lines'].sort

  FileUtils.mkdir_p(File.dirname(t.name))
  File.open(t.name, 'w') do |f|

    # Iterate over every point
    track.each do |point|

      # Get the thresholds for this point
      prob_trace = probability_threshold(point, cfg['prob_lines'])

      # Add the current point to the running average track
      trace_point = point.slice(:mile).merge({ prob_trace: prob_trace[:asc] })
      running_avg_track << trace_point

      # Remove points until the running average track is no longer greater than the required number of miles
      while (running_avg_track.last[:mile] - running_avg_track.first[:mile]) > miles_covered
        running_avg_track.shift
      end

      # Set up the average point
      avg = trace_point.dup

      # Find the most comfortable sleeping temperature in the running average track
      running_avg_track.each do |running_avg_pt|
        running_avg_pt[:prob_trace].each do |prob, temp|
          if (temp - cfg['sleep_comfort']).abs < (avg[:prob_trace][prob] - cfg['sleep_comfort']).abs
            avg[:prob_trace][prob] = temp
          end
        end
      end

      # Write the line to the file
      f << avg[:mile]
      f << ','
      f << asc_probs.map { |prob| avg[:prob_trace][prob] }.join(',')
      f << "\n"
    end
  end

  puts "STATISTIC #{t.name}"
end

# Combine best possible sleep comfort CSVs
file File.join(PCT_WEATHER_DATA_PATH, 'sleep_comfort.csv') => [DATE_CONFIG_FILE] + PCT_WEATHER_DATE_RANGE.map { |date| File.join(PCT_WEATHER_SLEEP_COMFORT_PATH, "#{date.to_s}.csv") } do |t|
  FileUtils.mkdir_p(File.dirname(t.name))
  File.open(t.name, 'w') do |f|
    t.prerequisites[1..-1].each do |day|
      File.foreach(day).each do |line|
        f << line
      end
    end
  end

  puts "COMBINE #{t.name}"
end
  
# Generate image grid columns (interpolated column for each point)
rule Regexp.new(File.join(PCT_WEATHER_IMAGE_GRID, '\d{4}-\d{2}-\d{2}.csv')) => proc { |tn| image_grid_prereqs(tn) } do |t|
  subtrack = YAML.load(File.read(t.prerequisites[0]))
  img_cfg = YAML.load(File.read(t.prerequisites[1]))
  temp_cfg = YAML.load(File.read(t.prerequisites[2]))

  FileUtils.mkdir_p(File.dirname(t.name))
  File.open(t.name, 'w') do |f|

    # Interpolate down the columns
    subtrack.each do |point|
      f << "#{point[:mile]},"
      prob_column = []

      prev_temp, prev_prob = point[:temp_probs].first
      row_idx = 0
      row_temp = temp_cfg['min_temp']
      point[:temp_probs].to_a[1..-1].each do |temp, prob|
        while row_temp <= temp_cfg['max_temp'] and temp > row_temp
          prob_column << ((row_temp - prev_temp) / (temp - prev_temp)) * (prob - prev_prob) + prev_prob
          row_idx += 1
          row_temp = (row_idx.to_f / img_cfg['height']) * (temp_cfg['max_temp'] - temp_cfg['min_temp']) + temp_cfg['min_temp']
        end
        prev_temp = temp
        prev_prob = prob
      end

      f << "#{prob_column.join(',')}\n"
    end
  end
  puts "INTERPOLATE #{t.name}"
end

# Combine image grid columns into a single track
file File.join(PCT_WEATHER_DATA_PATH, 'combined_image_grid_cols.csv') => [DATE_CONFIG_FILE] + PCT_WEATHER_DATE_RANGE.map { |date| File.join(PCT_WEATHER_IMAGE_GRID, "#{date.to_s}.csv") } do |t|
  FileUtils.mkdir_p(File.dirname(t.name))
  File.open(t.name, 'w') do |f|
    t.prerequisites[1..-1].each do |pr|
      File.foreach(pr) do |line|
        f << line
      end
    end
  end
  puts "COMBINE #{t.name}"
end

# Generate image grid (fully interpolated)
file File.join(PCT_WEATHER_DATA_PATH, 'image_grid.csv') => [File.join(PCT_WEATHER_DATA_PATH, 'combined_image_grid_cols.csv'), IMAGE_CONFIG_FILE] do |t|
  img_cfg = YAML.load(File.read(t.prerequisites[1]))
  File.open(t.prerequisites[0]) do |source|
    source.seek(-1, IO::SEEK_END)

    # Get the total miles
    while %W[\r \n].include?(source.readchar)
      source.seek(-2, IO::SEEK_CUR)
    end
    source.seek(-1, IO::SEEK_CUR)
    line = ''
    char = nil
    until %W[\r \n].include?(char = source.readchar)
      line = char + line
      source.seek(-2, IO::SEEK_CUR)
    end
    mile, rows = line.split(',', 2)
    total_miles = mile.to_f
    source.seek(0, IO::SEEK_SET)

    # Interpolate across the rows
    FileUtils.mkdir_p(File.dirname(t.name))
    File.open(t.name, 'w') do |dest|
      # Interpolate across the rows
      prev_mile = mile
      prev_rows = rows
      col_idx = 0
      col_mile = 0

      source.each_line.with_index do |line, line_no|
        fields = line.split(',').map(&:to_f)
        point_mile = fields[0]
        point_rows = fields[1..-1]
        if line_no > 0
          while col_mile <= total_miles and point_mile > col_mile
            frac = (col_mile - prev_mile) / (point_mile - prev_mile)
            dest << point_rows.map.with_index do |prob, row|
              frac * (prob - prev_rows[row]) + prev_rows[row]
            end.join(',') + "\n"
            col_idx += 1
            col_mile = (col_idx.to_f / img_cfg['width']) * total_miles
          end
        end
        prev_mile = point_mile
        prev_rows = point_rows
      end
    end
  end

  puts "INTERPOLATE #{t.name}"
end

# Generate image
file File.join(PCT_WEATHER_DATA_PATH, 'temp_probs.png') => [COLOR_CONFIG_FILE, IMAGE_CONFIG_FILE, File.join(PCT_WEATHER_DATA_PATH, 'image_grid.csv')] do |t|
  color_cfg = YAML.load(File.read(t.prerequisites[0]))
  img_cfg = YAML.load(File.read(t.prerequisites[1]))
  png = ChunkyPNG::Image.new(img_cfg['width'], img_cfg['height'], ChunkyPNG::Color::TRANSPARENT)

  File.foreach(t.prerequisites[2]).with_index do |line, col|
    puts "Rendering column #{col}..."
    line.split(',').map(&:to_f).each_with_index do |value, row|
      if ((col.to_f / color_cfg['gridlines']['horizontal']['weight']).round % (color_cfg['gridlines']['horizontal']['interval'] / color_cfg['gridlines']['horizontal']['weight']).round == 0)
        alpha = 255
        red = color_cfg['gridlines']['horizontal']['red']
        green = color_cfg['gridlines']['horizontal']['green']
        blue = color_cfg['gridlines']['horizontal']['blue']
      elsif ((row.to_f / color_cfg['gridlines']['vertical']['weight']).round % (color_cfg['gridlines']['vertical']['interval'] / color_cfg['gridlines']['vertical']['weight']).round == 0)
        alpha = 255
        red = color_cfg['gridlines']['vertical']['red']
        green = color_cfg['gridlines']['vertical']['green']
        blue = color_cfg['gridlines']['vertical']['blue']
      else
        alpha = (255 * value).round
        color_region = ((row.to_f / img_cfg['height']) * (color_cfg['set_points'].length - 1))
        color_ceil = color_cfg['set_points'][color_region.ceil]
        color_floor = color_cfg['set_points'][color_region.floor]
        color_region_frac = color_region - color_region.floor
        red, green, blue = %w[red green blue].map { |component| (color_floor[component] + color_region_frac * (color_ceil[component] - color_floor[component])).round }
      end

      png[col, img_cfg['height'] - 1 - row] = ChunkyPNG::Color.rgba(red, green, blue, alpha)
    end
  end

  FileUtils.mkdir_p(File.dirname(t.name))
  puts "Saving image..."
  png.save(t.name)
  puts "IMAGE #{t.name}"
end
