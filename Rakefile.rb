raise 'Need to be on jruby!' if RUBY_PLATFORM != 'java'

require 'bundler'
begin
  Bundler.setup(:default, :development)
rescue Bundler::BundlerError => e
  raise LoadError.new("Unable to Bundler.setup(): You probably need to run `bundle install`: #{e.message}")
end

require 'date'
require 'nokogiri'
require 'distribution'
require 'active_support'
require 'concurrent'
require 'chunky_png'

# Setup require path
$LOAD_PATH.unshift(*Dir.glob('lib/**/').map { |r| File.expand_path(r) })

Rake.application.options.always_multitask = true
Rake.application.rake_require 'utils'
require 'color_spectrum'
rake_require 'normal_weather'
rake_require 'track'
rake_require 'pct_weather'
rake_require 'elevation'
rake_require 'bible'
rake_require 'pct_bible'
